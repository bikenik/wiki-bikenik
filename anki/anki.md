# [Anki](https://apps.ankiweb.net)

Anki is an awesome app that uses [spaced repetition learning](http://www.wikiwand.com/en/Spaced_repetition) to memorise and learn things efficiently.

## Plugins

- [Advanced Browser](https://ankiweb.net/shared/info/874215009)
 -- add more features to the card browser.
- [Mini Format Pack](https://ankiweb.net/shared/info/295889520) This is a quick stripped-down version of Stefan van den Akker's Power Format Pack add-on. *Only supports Anki 2.1.x.*
- [Syntax Highlighting for Code](https://ankiweb.net/shared/info/1463041493) Allows you to insert syntax-highlighted code snippets into your notes. *Supports both Anki 2.0.x and 2.1.x.*

## Tips

## Notes

## Links
