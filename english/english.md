# English

To make subtitles is better use following solutions:

-   Pragmatic Segmenter - [live demo](https://www.tm-town.com/natural-language-processing), [github](https://github.com/diasks2/pragmatic_segmenter)
-   "[aeneas](https://www.readbeyond.it/aeneas/)" is a Python/C library and a set of tools to automagically synchronize audio and text (aka forced alignment) - [web app](https://aeneasweb.org/status), [github](https://github.com/readbeyond/aeneas/)
